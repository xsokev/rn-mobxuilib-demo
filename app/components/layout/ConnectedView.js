import React from 'react';
import {inject, observer} from 'mobx-react/native';
import Container from './Container';
import NoConnection from '../empty/NoConnection';
import {colors} from '@uikit/styles';

const ConnectedView = ({ ui, children, ...props }) => {
  return (
    <Container nopad {...props}>
      {!ui.isConnected ? <NoConnection message="No connection to internet!" color={colors.red200} /> : null }
      {children}
    </Container>
  )
}

export default inject("ui")(observer(ConnectedView));
