import React from 'react';
import {StyleSheet} from 'react-native';
import {Colors, View} from 'react-native-ui-lib';

export default ({vertical, length, color}) => {
	const styles = {
		height: vertical ? (length || '100%') : 0,
		width: vertical ? 0 : (length || '100%'),
		borderBottomWidth: vertical ? 0 : StyleSheet.hairlineWidth,
		borderBottomColor: color || Colors.dark60,
		borderRightWidth: vertical ? StyleSheet.hairlineWidth : 0,
		borderRightColor: color || Colors.dark60
	}
	return (
		<View style={styles}></View>
	)
}
