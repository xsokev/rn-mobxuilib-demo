import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Constants} from 'react-native-ui-lib';

const IPhoneXMaxDim = 812;
const isIPhoneX = Constants.isIOS && (Constants.screenHeight === IPhoneXMaxDim || Constants.screenWidth === IPhoneXMaxDim);
const HEIGHT = Constants.isIOS ?
  isIPhoneX ? 44 : 20
  : Constants.statusBarHeight;
const styles = StyleSheet.create({
  cover: {
    height: HEIGHT,
    width: Constants.screenWidth,
    zIndex: 10,
    position: 'absolute',
    top: 0,
    left: 0
  }
})

export default ({color}) => {
  return (<View style={[styles.cover, {backgroundColor: color || 'transparent'}]}></View>)
}
