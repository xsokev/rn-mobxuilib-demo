import React from 'react';
import {View, Image, Constants} from 'react-native-ui-lib';

export default ({children, source, mode}) => {
  const imageStyles = {
    position: 'absolute',
    left: 0, 
    right: 0,
    top: 0,
    bottom: 0,
    width: Constants.screenWidth, 
    height: Constants.screenHeight
  }
  return (
    <View flex>
      <Image style={imageStyles} source={source} resizeMode={mode || 'cover'} />
      {children}
    </View>
  )
}
