import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {Colors, View} from 'react-native-ui-lib';
import StateView from './StateView';
import Divider from '../layout/Divider';

export default ({list, loading, refreshing, empty, data, renderListItem, emptyComponent, noborder, message}) => {
	const barStyle = {
		flex: 0,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.rgba(0,0,0,0),
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.orange60,
		backgroundColor: Colors.orange80
	};
	return (
		<View flex>
			{
				(loading === true) ? (
					<StateView color={Colors.dark20} message={list.state+" "+(message || '')} style={barStyle} indicator />
				) : null
			}
			<FlatList
				data={data}
				renderItem={renderListItem}
				keyExtractor={(item, i) => i}
				onRefresh={list.refresh && list.refresh.bind(list)}
				refreshing={refreshing}
				ItemSeparatorComponent={noborder ? null : Divider}
			/>
		</View>
	)
}
