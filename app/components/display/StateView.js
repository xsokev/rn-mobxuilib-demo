import React from 'react';
import {View, Text, Colors} from 'react-native-ui-lib';

export default ({message, color, ...props}) => (
  <View paddingH-15 paddingV-10 {...props}>
    <Text color={color || Colors.dark40}>{message}</Text>
  </View>
)
