import React from 'react';
import {StyleSheet} from 'react-native';
import {Colors, View, Text} from 'react-native-ui-lib';
import Icon from 'react-native-vector-icons/Ionicons';

export default ({icon, label, color, size}) => {
	const DEFAULTSIZE = size || 18;
	const DEFAULTCOLOR = color || Colors.dark30;
	const styles = StyleSheet.create({
		container: {

		},
		icon: {
			paddingHorizontal: 8,
			fontSize: DEFAULTSIZE + 8,
			color: DEFAULTCOLOR
		},
		label: {
			fontSize: DEFAULTSIZE,
			color: DEFAULTCOLOR
		}
	});
	return (
		<View row center style={styles.container}>
			{icon ? <Icon name={icon} style={styles.icon} /> : null }
			{label ? <Text style={styles.label}>{label}</Text> : null }
		</View>
	)
}
