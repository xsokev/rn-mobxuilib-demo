import React from 'react';
import {observer, inject} from 'mobx-react/native';
import { View, Text } from 'react-native-ui-lib';

import {colors, width, height} from '../../util/styles';

@inject(stores => ({
  primary: stores.theme.primary,
  primaryText: stores.theme.primaryText,
}))
@observer
export default class About extends React.Component {
  static navigationOptions = {
    title: 'About'
  };
  render(){
    const {primary, primaryText, navigation} = this.props;
    return (
      <View flex center paddingH-25>
        <Text text20 dark20 center>About</Text>
        <Text text80 dark20 center>This is a simple starting point for creating React Native Apps using Mobx, React Navigation, and React Native UI Lib.</Text>
      </View>
    )
  }
}
