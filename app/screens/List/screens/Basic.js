import React from 'react';
import {StyleSheet} from 'react-native';
import {observer, inject} from 'mobx-react/native';
import {Constants, Colors} from 'react-native-ui-lib';
import {FlatList} from '@uikit';
import ListItem from '../components/ListItemBasic';

@inject(stores => ({
  primary: stores.theme.primary,
  primaryText: stores.theme.primaryText,
  accent: stores.theme.accent,
  background: stores.theme.background,
  people: stores.people
}))
@observer
export default class Basic extends React.Component {
  static navigationOptions = {
    title: 'Basic List'
  };
  render(){
    const {people} = this.props;
    return (
      <FlatList 
        list={people} 
        loading={people.loading} 
        refreshing={people.refreshing} 
        empty={people.empty} 
        emptyComponent={null} 
        noborder={false} 
        message={``}
        data={people.itemsSorted}
        renderListItem={this._renderListItem}
      />
    );
  }
  _renderListItem({ item, index }){
    return (
      <ListItem
        text={item.name}
        icon={`ios-${item.gender.toLowerCase()}`}
        onPress={() => {}}
      />
    )
  }
}

const styles = StyleSheet.create({
  imageContainer: {
  },
});