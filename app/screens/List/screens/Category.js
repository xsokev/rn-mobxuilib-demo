import React from 'react';
import {StyleSheet} from 'react-native';
import {observer, inject} from 'mobx-react/native';
import {Constants, Colors} from 'react-native-ui-lib';
import {FlatList} from '@uikit';
import ListItem from '../components/ListItemContact';

@inject(stores => ({
  primary: stores.theme.primary,
  primaryText: stores.theme.primaryText,
  accent: stores.theme.accent,
  background: stores.theme.background,
  sports: stores.sports
}))
@observer
export default class Category extends React.Component {
  static navigationOptions = {
    title: 'Category List'
  };
  render(){
    const {sports} = this.props;
    return (
      <FlatList 
        list={sports} 
        loading={sports.loading} 
        refreshing={sports.refreshing} 
        empty={sports.empty} 
        emptyComponent={null} 
        noborder={false} 
        message={``}
        data={sports.itemsSorted}
        renderListItem={this._renderListItem}
      />
    );
  }
  _renderListItem({ item, index }){
    return (
      <ListItem
        text={item.name}
        location={item.location}
        organization={item.organization}
        avatar={`${item.avatar}`}
        type={`${item.type}`}
        onPress={() => {}}
      />
    )
  }
}

const styles = StyleSheet.create({
  imageContainer: {
  },
});