import {observable, action, computed} from 'mobx';
import format from 'date-fns/format';
import CustomField from './CustomField';
import {DATE_FORMAT, TIME_FORMAT} from '../../config';
import {prepDate} from '@uikit/utils';

export default class Event {
  @observable _id = 0;
  @observable venue = '';
  @observable start = '';
  @observable end = '';
  @observable allDay = false;
  @observable description = '';
  @observable featuredImage = '';
  @observable order = 0;
  @observable type = '';
  @observable title = '';
  @observable facilitators = [];
  @observable invitees = [];
  @observable attendees = [];

  constructor(json) {
    this._id = json._id || 0;
    this.venue = json.venue || '';
    this.start = json.start || '';
    this.end = json.end || '';
    this.allDay = json.allDay || false;
    this.description = json.description || '';
    this.featuredImage = json.featuredImage || '';
    this.title = json.title || '';
    this.type = json.type || '';
    this.order = json.order || 0;

    let fas = [];
    let ins = [];
    let ats = [];

    if(json.facilitators && json.facilitators.length > 0){
      json.facilitators.forEach(f => {
        fas.push({_id: f._id, name: f.name, avatar: f.avatar});
      });
    }
    this.facilitators.replace(fas);

    if(json.invitees && json.invitees.length > 0){
      json.invitees.forEach(i => {
        ins.push({_id: i._id, name: i.name, avatar: i.avatar});
      });
    }
    this.invitees.replace(ins);

    if(json.attendees && json.attendees.length > 0){
      json.attendees.forEach(a => {
        ats.push({_id: a._id, name: a.name, avatar: a.avatar});
      });
    }
    this.attendees.replace(ats);
  }

  @computed get eventType(){
    return CustomField.event_type(this.customFields);
  }
  @computed get startDate(){
    return format(prepDate(this.start), DATE_FORMAT);
  }
  @computed get startTime(){
    return format(prepDate(this.start), TIME_FORMAT);
  }
  @computed get endDate(){
    return format(prepDate(this.end), DATE_FORMAT);
  }
  @computed get endTime(){
    return format(prepDate(this.end), TIME_FORMAT);
  }
  @computed get startDay(){
    return format(prepDate(this.start), 'dddd');
  }
  @computed get longDate(){
    return format(prepDate(this.start), 'dddd, MMMM Do, YYYY @ h:mma');
  }
  toString(start){
    let msg = start + this.title + ' @ ' + this.startTime;
    if(this.facilitators && this.facilitators.length > 0){
      msg += '\nArtists: ' + this.facilitators.map(f => f.name).join(' and ');
    }
    return msg;
  }
}
