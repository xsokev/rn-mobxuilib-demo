import {observable, computed} from 'mobx';

export default class Person {
  static get type() {
    return 'person';
  }
  @observable _id = 0;
  @observable active = true;
  @observable bio = '';
  @observable avatar = '';
  @observable username = '';
  @observable firstname = '';
  @observable lastname = '';
  @observable gender = '';
  @observable email = '';

  constructor(json) {
    this._id = json._id || 0;
    this.active = json.active || true;
    this.bio = json.bio || '';
    this.avatar = json.avatar || '';
    this.username = json.username || '';
    this.firstname = json.firstname || '';
    this.lastname = json.lastname || '';
    this.gender = json.gender || '';
    this.email = json.email || '';
  }

  @computed get name(){
    return [this.firstname, this.lastname].join(' ');
  }
}
