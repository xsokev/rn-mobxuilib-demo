import {observable, computed} from 'mobx';

export default class Restaurant {
  @observable _id = 0;
  @observable name = '';
  @observable cuisine = '';
  @observable description = '';
  @observable address = '';
  @observable city = '';
  @observable state = '';
  @observable zip = '';
  @observable phone = '';
  @observable hours = '';
  @observable menu = '';
  @observable reservations = '';
  @observable image = '';

  constructor(json){
    this._id = json.id || '';
    this.name = json.name || '';
    this.cuisine = json.cuisine || '';
    this.description = json.description || '';
    this.address = json.address || '';
    this.city = json.city || '';
    this.state = json.state || '';
    this.zip = json.zip || '';
    this.phone = json.phone || '';
    this.hours = json.hours || '';
    this.menu = json.menu || '';
    this.reservations = json.reservations || '';
    this.image = json.image || '';
  }
  @computed get acsz(){
    return `${this.address}, ${this.city}, ${this.state} ${this.zip}`
  }
}
