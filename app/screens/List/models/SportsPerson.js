import {observable, computed} from 'mobx';
import Person from './Person';

export default class SportsPerson extends Person {
  static get type() {
    return 'sportsperson';
  }
  @observable city = '';
  @observable state = '';
  @observable organization = '';
  @observable type = '';

  constructor(json) {
    super(json);
    this.city = json.city || '';
    this.state = json.state || '';
    this.organization = json.organization || '';
    this.type = json.type || '';
  }

  @computed get location(){
    return [this.city, this.state].join(', ');
  }
}
