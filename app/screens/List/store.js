import {AppState, Alert} from 'react-native';
import { observable, action, computed, observe } from 'mobx';
import { Assets } from 'react-native-ui-lib';
import { APPLICATION_TITLE } from '../../config';
import StateStore from '../../stores/StateStore';

const sortBy = (array, prop) => {
  return array.sort((a, b) => {
    if(a[prop] < b[prop]){
      return -1;
    } else if(a[prop] > b[prop]){
      return 1;
    }
    return 0;
  })
}

export default class Store extends StateStore {
  @observable items = [];
  @observable categories = [];
  @observable filter = '';
  @observable filterProperty = 'name';
  @observable selectedCategory = 'All';
  @observable categoryProperty = 'category';
  @observable sortProperty = 'firstname';

  constructor(Model, APIRequests, apiTransport, storageTransport, ui) {
    super(apiTransport, storageTransport, ui, Model, APIRequests);
    if(Model && Model.type){
      this.storage && this.storage.load(Model.type)
      .then(storedata => {
        if(storedata){
          let _data = JSON.parse(storedata);
          if(_data){
            try {
              let newData = _data.map(i => new Model(i));
              this.items.replace(newData);
            } catch(e) {
              this.storage.remove(Model.type);
            }
          }
        }
      })
      .then(() => this.state = 'done')
    }
  }
  @action loadData() {
    const Model = this.Model;
    if(this.APIRequests && this.APIRequests.load && Model){
      return this.api.get(this.APIRequests.load)
        .then((data) => {
          this.storage.save(Model.type, JSON.stringify(data));
          this.state = 'done';
          let newData = data.map(i => new Model(i));
          this.items.replace(newData);
        })
        .catch((response) => {
          this.state = 'error';
        });
    } else {
      this.state = 'error';      
    }
  }
  @action load() {
    super.load();
    this.loadData();
  }
  @action refresh() {
    super.refresh();
    if(this.ui.isConnected){
      this.loadData();
    }
  }
  @computed get filterPlaceholder(){
    return `filter ${this.Model.type}`
  }
  @computed get main(){
    if(this.items.length > 0){
      return this.items[0];
    }
  }
  @computed get itemsSorted(){
    return this.sortProperty === '' ? this.items : sortBy(this.items, this.sortProperty);
  }
  @computed get itemsFiltered(){
    const items = this.itemsSorted;
    let filtered = items.filter( item => item[this.filterProperty].toLowerCase().indexOf(this.filter.toLowerCase()) >= 0 )
    return this.filter.length ? filtered : items;
  }
  @computed get itemsFilteredByCategory(){
    const items = this.itemsByCategory;
    let filtered = items.filter( item => item[this.filterProperty].toLowerCase().indexOf(this.filter.toLowerCase()) >= 0 )
    return this.filter.length ? filtered : items;
  }
  @computed get itemsByCategory(){
    const hasCat = this.selectedCategory !== '' && this.selectedCategory.toLowerCase() !== 'all'
    return hasCat ? this.itemsSorted.filter(item => item[this.categoryProperty] && item[this.categoryProperty] === this.selectedCategory) : this.itemsSorted;
  }
}
