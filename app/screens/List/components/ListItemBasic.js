import React from 'react';
import {Colors, ListItem, Text} from 'react-native-ui-lib';
import Icon from 'react-native-vector-icons/Ionicons';

export default ({icon, text, color, ...props}) => (
  <ListItem {...props}>
    { icon ?
      <ListItem.Part left containerStyle={{marginLeft: 16}}>
        <Icon name={icon} color={color || Colors.dark20} size={24} />
      </ListItem.Part>
    : null }
    <ListItem.Part middle containerStyle={{marginHorizontal: 10}}>
      <Text style={{color: color || Colors.dark20}} text70>{text}</Text>
    </ListItem.Part>
  </ListItem>
)