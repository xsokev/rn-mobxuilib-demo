import React from 'react';
import {Colors, ListItem, View, Text, Avatar} from 'react-native-ui-lib';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {IconLabel} from '@uikit';

const iconStyle = {
  top: 0,
  left: 0,
}

export default ({text, location, organization, avatar, type, color, ...props}) => (
  <ListItem {...props}>
    <ListItem.Part left>
      <View row center paddingH-15>
        <View center br100 padding-5 style={{backgroundColor: type.toLowerCase() === 'fan' ? Colors.green30 : Colors.blue30, marginRight: 10}}>
          <Icon name={type.toLowerCase() === 'fan' ? 'football' : 'thumb-up-outline'} color={Colors.white} size={18} />
        </View>
        <Avatar imageSource={{uri: avatar}} />
      </View>
    </ListItem.Part>
    <ListItem.Part middle>
      <View paddingV-15>
        <Text text50 style={{color: Colors.red20}}>{text}</Text>
        <View row>
          <IconLabel icon="ios-globe" label={location} size={15} />
          {/* <IconLabel icon="ios-contact" label={organization} size={15} /> */}
        </View>
      </View>
    </ListItem.Part>
    <ListItem.Part right>
    </ListItem.Part>
  </ListItem>
)