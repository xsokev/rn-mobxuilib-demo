import React from 'react';
import {View, Text} from 'react-native-ui-lib';
import * as Animatable from 'react-native-animatable';
import {Icon} from '../';

export default ({icon, temp, day, index}) => (
  <Animatable.View animation="fadeInRight" delay={index*100} duration={400} easing="ease-in-out">
    <View center padding-5 marginH-10>
      <Text shade text90>{day.toUpperCase()}</Text>
      <View paddingV-10><Icon name={icon} size={30} /></View>
      <Text>{temp}</Text>              
    </View>
  </Animatable.View>
)