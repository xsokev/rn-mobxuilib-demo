import React from 'react';
import {View, Text, TouchableOpacity, Colors} from 'react-native-ui-lib';
import format from 'date-fns/format';
import IIcon from 'react-native-vector-icons/Ionicons';
import {Icon} from '../';

const buttonStyle = {
  backgroundColor: 'transparent',
  height: 30
}

export default ({weather}) => (
  <View row center paddingV-20 paddingH-15 bg-shade>
    <View row left top>
      <Text h1 white>{weather.tempurature}</Text>
      <Icon name={weather.units && weather.units.tempurature === 'C' ? 'degree-celcius' : 'degree-fahrenheit'} size={20} color={Colors.white} />
    </View>
    <View paddingH-5 center>
      <Icon name={`${weather.icon}-o`} size={60} color={Colors.white} />
    </View>
    <View left centerV>
      <Text text50 white>{weather.location.toUpperCase()}</Text>              
      <Text text90 white>{format(weather.buildDate, 'dddd, MMMM D').toLocaleUpperCase()}</Text>              
    </View>
    {/* <View paddingR-15 right bg-primary>
      <TouchableOpacity style={buttonStyle}>
        <IIcon name="md-refresh" color={Colors.white} size={25} />
      </TouchableOpacity>
    </View> */}
  </View>
)