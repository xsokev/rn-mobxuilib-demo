import React from 'react';
import {View, Text} from 'react-native-ui-lib';
import {Icon} from '../';

export default ({icon, text}) => (
  <View row center padding-5 marginH-10>
    <Icon name={icon} size={20} />
    <Text>{text}</Text>              
  </View>
)