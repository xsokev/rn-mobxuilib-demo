export const WEATHER_CITY         = 'memphis.tn';
// export const WEATHER_CITY_IMAGE   = `https://kevinandre.com/app/weather/${WEATHER_CITY}/background.jpg`;
export const API_WEATHER          = () => `https://kevinandre.com/app/weather/${WEATHER_CITY}/current.json`;
