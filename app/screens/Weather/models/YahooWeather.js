import Weather from './Weather';
import Forecast from './Forecast';

export default class YahooWeather extends Weather {
  constructor(json) {
    super(json)
    this.units = json.units || {
      distance: "mi",
      pressure: "in",
      speed: "mph",
      temperature: "F"
    };
    this.buildDate = json.lastBuildDate || '';
    const wind = json.wind;
    if(wind){
      this.windChill = wind.chill || 0;
      this.windDirection = wind.direction || 0;
      this.windSpeed = wind.speed || 0;
    }
    const atmosphere = json.atmosphere;
    if(atmosphere){
      this.humidity = atmosphere.humidity || 0;
      this.pressure = atmosphere.pressure || 0;
      this.rising = atmosphere.rising || 0;
      this.visibility = atmosphere.visibility || 0;
    }
    const astronomy = json.astronomy;
    if(astronomy){
      this.sunrise = astronomy.sunrise || '';
      this.sunset = astronomy.sunset || '';
    }
    const location = json.location;
    if(location){
      this.location = [location.city, location.region].join(', ');
    }

    const item = json.item;
    if(item){
      this.latitude = item.lat || 0;
      this.longitude = item.long || 0;

      const condition = item.condition;
      if(condition){
        this.conditionCode = condition.code || 0;
        this.tempurature = condition.temp || 0;
        this.condition = condition.text || '';
      }
    
      if(item.forecast){
        let _forecast = item.forecast
          .map(f => ({
            code: f.code,
            date: f.date,
            day: f.day,
            high: f.high,
            low: f.low,
            condition: f.text
          }))
          .map(f => new Forecast(f));
        this.forecast.replace(_forecast);
      }
    }
  }
}
