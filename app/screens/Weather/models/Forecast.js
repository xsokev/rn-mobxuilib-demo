import {observable, action, computed} from 'mobx';

export default class Forecast {
  @observable code = 0;
  @observable condition = '';
  @observable date = '';
  @observable day = '';
  @observable high = '';
  @observable low = '';

  constructor(json){
    this.code = json.code || 0;
    this.condition = json.condition || '';
    this.date = json.date || '';
    this.day = json.day || '';
    this.high = json.high || '';
    this.low = json.low || '';
  }
  @computed get icon(){
    switch(parseInt(this.code, 10)){
        case 0:	//tornado
        case 1:	//tropical storm
        case 2:	//hurricane
          return "tornado";
          break;
        case 3:	//severe thunderstorms
        case 4:	//	thunderstorms
          return "lightning";
          break;
        case 5:	//	mixed rain and snow
        case 6:	//	mixed rain and sleet
        case 7:	//	mixed snow and sleet
        case 8:	//	freezing drizzle
        case 10:	//freezing rain
        case 13:	//snow flurries
        case 14:	//light snow showers
        case 15:	//blowing snow
        case 16:	//snow
        case 17:	//hail
        case 18:	//sleet
        case 46:	//snow showers
          return "cloud-snow";
          break;
        case 9:	//	drizzle
        case 11:	//showers
        case 12:	//showers
          return "raindrops";
          break;
        case 19:	//dust
        case 20:	//foggy
        case 21:	//haze
        case 22:	//smoky
          return "cloud-fog";
          break;
        case 23:	//blustery
          return "cloud-wind";
          break;
        case 24:	//windy
          return "wind";
          break;
        case 26:	//cloudy
          return "clouds";
          break;
        case 27:	//mostly cloudy (night)
        case 29:	//partly cloudy (night)
          return "cloud-moon";
          break;
        case 28:	//mostly cloudy (day)
        case 30:	//partly cloudy (day)
          return "cloud-sun";
          break;
        case 31:	//clear (night)
          return "stars";
          break;
        case 33:	//fair (night)
          return "moon";
          break;
        case 32:	//sunny
        case 34:	//fair (day)
          return "sun";
          break;
        case 35:	//mixed rain and hail
          return "clouds";
          break;
        case 25:	//cold
          return "cold";
          break;
        case 36:	//hot
          return "hot";
          break;
        case 37:	//isolated thunderstorms
        case 38:	//scattered thunderstorms
        case 39:	//scattered thunderstorms
          return "cloud-lightning";
          break;
        case 40:	//scattered showers
        case 45:	//thundershowers
        case 47:	//isolated thundershowers
          return "cloud-rain";
          break;
        case 41:	//heavy snow
        case 42:	//scattered snow showers
        case 43:	//heavy snow
          return "cloud-snowflakes";
          break;
        case 44:	//partly cloudy
          return "cloud";
        default:
          return "sun";
    }
  }
}