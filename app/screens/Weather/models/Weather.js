import {observable, action, computed} from 'mobx';

export default class Weather {
  @observable location = '';
  @observable units = {};
  @observable buildDate = '';
  @observable windChill = 0;
  @observable windDirection = 0;
  @observable windSpeed = 0;
  @observable humidity = 0;
  @observable pressure = 0;
  @observable rising = 0;
  @observable visibility = 0;
  @observable sunrise = "";
  @observable sunset = "";
  @observable latitude = 0;
  @observable longitude = 0;
  @observable conditionCode = 0;
  @observable tempurature = 0;
  @observable condition = '';
  @observable forecast = [];

  constructor(json) {
    
  }
  @computed get icon(){
    switch(parseInt(this.conditionCode, 10)){
        case 0:	//tornado
        case 1:	//tropical storm
        case 2:	//hurricane
          return "tornado";
          break;
        case 3:	//severe thunderstorms
        case 4:	//	thunderstorms
          return "lightning";
          break;
        case 5:	//	mixed rain and snow
        case 6:	//	mixed rain and sleet
        case 7:	//	mixed snow and sleet
        case 8:	//	freezing drizzle
        case 10:	//freezing rain
        case 13:	//snow flurries
        case 14:	//light snow showers
        case 15:	//blowing snow
        case 16:	//snow
        case 17:	//hail
        case 18:	//sleet
        case 46:	//snow showers
          return "cloud-snow";
          break;
        case 9:	//	drizzle
        case 11:	//showers
        case 12:	//showers
          return "raindrops";
          break;
        case 19:	//dust
        case 20:	//foggy
        case 21:	//haze
        case 22:	//smoky
          return "cloud-fog";
          break;
        case 23:	//blustery
          return "cloud-wind";
          break;
        case 24:	//windy
          return "wind";
          break;
        case 26:	//cloudy
          return "clouds";
          break;
        case 27:	//mostly cloudy (night)
        case 29:	//partly cloudy (night)
          return "cloud-moon";
          break;
        case 28:	//mostly cloudy (day)
        case 30:	//partly cloudy (day)
          return "cloud-sun";
          break;
        case 31:	//clear (night)
          return "stars";
          break;
        case 33:	//fair (night)
          return "moon";
          break;
        case 32:	//sunny
        case 34:	//fair (day)
          return "sun";
          break;
        case 35:	//mixed rain and hail
          return "clouds";
          break;
        case 25:	//cold
          return "cold";
          break;
        case 36:	//hot
          return "hot";
          break;
        case 37:	//isolated thunderstorms
        case 38:	//scattered thunderstorms
        case 39:	//scattered thunderstorms
          return "cloud-lightning";
          break;
        case 40:	//scattered showers
        case 45:	//thundershowers
        case 47:	//isolated thundershowers
          return "cloud-rain";
          break;
        case 41:	//heavy snow
        case 42:	//scattered snow showers
        case 43:	//heavy snow
          return "cloud-snowflakes";
          break;
        case 44:	//partly cloudy
          return "cloud";
        default:
          return "sun";
    }
  }
  @computed get wind(){
    return `${this.windSpeed} ${this.units.speed}`
  }
  @computed get direction(){
    if(this.windDirection >= 337.5 && this.windDirection < 22.5){
      return {abbr:"N", name:"North"};
    } else if(this.windDirection >= 22.5 && this.windDirection < 67.5){
      return {abbr:"NE", name:"North East"};
    } else if(this.windDirection >= 67.5 && this.windDirection < 112.5){
      return {abbr:"E", name:"East"};
    } else if(this.windDirection >= 112.5 && this.windDirection < 157.5){
      return {abbr:"SE", name:"South East"};
    } else if(this.windDirection >= 157.5 && this.windDirection < 202.5){
      return {abbr:"S", name:"South"};
    } else if(this.windDirection >= 202.5 && this.windDirection < 247.5){
      return {abbr:"SW", name:"South West"};
    } else if(this.windDirection >= 247.5 && this.windDirection < 292.5){
      return {abbr:"W", name:"West"};
    } else if(this.windDirection >= 292.5 && this.windDirection < 337.5){
      return {abbr:"NW", name:"North West"};
    }
  }
  @computed get tempuratureUnit(){
    if(this.units.tempurature === 'C'){
      return "\u2103";
    } else {
      return "\u2109";
    }
  }
  toString(start){

  }
}
