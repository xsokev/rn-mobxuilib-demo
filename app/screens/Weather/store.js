import {AppState, Alert} from 'react-native';
import { observable, action, computed, observe } from 'mobx';
import { Assets } from 'react-native-ui-lib';
import { APPLICATION_TITLE } from '../../config';
import { API_WEATHER, WEATHER_CITY_IMAGE } from './config';
import StateStore from '../../stores/StateStore';
import Model from './models/YahooWeather';

export default class Store extends StateStore {
  @observable state = 'initiating';
  @observable data = null;

  constructor(apiTransport, storageTransport, ui) {
    super(apiTransport, storageTransport, ui);
    this.storage.load('weather')
      .then(storedata => {
        if(storedata){
          let _data = JSON.parse(storedata);
          this.data = new Model(_data);
        }
      })
      .then(() => this.state = 'done')
    Assets.loadAssetsGroup('weather', {
      'memphis.tn': require('./assets/images/memphis.tn.jpg'),
      'parkcity.ut': require('./assets/images/parkcity.ut.jpg'),
    });
  }
  @action loadData() {
    return this.api.get(API_WEATHER())
      .then((data) => {
        this.storage.save('weather', JSON.stringify(data));
        this.state = 'done';
        this.data = new Model(data);
      })
      .catch((response) => {
        this.state = 'error';
      });
  }
  @action load() {
    super.load();
    this.loadData();
  }
  @action refresh() {
    super.refresh();
    if(this.ui.isConnected){
      this.loadData();
    }
  }
}
