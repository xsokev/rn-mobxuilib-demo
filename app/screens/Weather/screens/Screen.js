import React from 'react';
import {StyleSheet, FlatList} from 'react-native';
import {observer, inject} from 'mobx-react/native';
import {Assets, View, Text, Image, Constants, Colors} from 'react-native-ui-lib';
import {WeatherHeader, WeatherInfo, Forecast} from '../components';
import {WEATHER_CITY} from '../config';

@inject(stores => ({
  primary: stores.theme.primary,
  primaryText: stores.theme.primaryText,
  accent: stores.theme.accent,
  background: stores.theme.background,
  weather: stores.weather
}))
@observer
export default class Weather extends React.Component {
  static navigationOptions = {
    title: 'Weather Demo'
  };
  render(){
    const {weather, primary, primaryText, accent, background, navigation} = this.props;
    let content = (      
      <View bg-white center padding-20 flex>
        <Text h1 grey>loading weather...</Text>
      </View>
    )
    if(weather.empty){
      content = (
        <View bg-white center padding-20 flex>
          <Text h1 red>no weather available</Text>
        </View>
      )
    } else if(weather.data !== null){
      content = this._renderWeather();
    }
    return content;
  }
  _renderForecast({ item, index }){
    return (
      <Forecast 
        temp={`${item.high}/${item.low}\u00B0`} 
        icon={`${item.icon}-o`} 
        day={item.day}
        index={index}
      />
    )
  }
  _renderWeather(){
    const {weather} = this.props;
    return (
      <View bg-white top flex>
        <View flex bottom style={styles.imageContainer}>
          <Image assetName={WEATHER_CITY} assetGroup="weather" style={styles.image} />
          <WeatherHeader weather={weather.data} />
        </View>
        <View row center style={styles.weatherInfo}>
          <WeatherInfo icon="flag" text={weather.data.wind.toUpperCase()} />
          <WeatherInfo icon="compass" text={weather.data.direction.name.toUpperCase()} />
        </View>
        <View style={styles.forecast}>
          <FlatList horizontal
            keyExtractor={(item, index) => item.date}
            data={weather.data.forecast}
            renderItem={this._renderForecast}
          />
        </View>
      </View>
    )
  }
}

const CONTAINER_HEIGHT = 300;
const styles = StyleSheet.create({
  imageContainer: {
  },
  image: {
    position: 'absolute',
    height: '100%',
    resizeMode: 'cover'
  },
  weatherInfo: {
    height: 70,
    backgroundColor: Colors.dark70
  },
  forecast: {
    height: 150,
    backgroundColor: Colors.dark80
  }
});