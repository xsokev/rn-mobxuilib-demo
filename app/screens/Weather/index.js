import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import Store from './store';
import Screen from './screens/Screen';
import Widget from './Widget';

import icoMoonConfig from './assets/selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export {
  Store,
  Screen,
  Icon,
  Widget,
}
