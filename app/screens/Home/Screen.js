import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {observer, inject} from 'mobx-react/native';
import { ListItem, View, Text, Button, ThemeManager } from 'react-native-ui-lib';

@inject(stores => ({
  primary: stores.theme.primary,
  primaryText: stores.theme.primaryText,
}))
@observer
export default class Home extends React.Component {
  static navigationOptions = {
    title: 'Demos'
  };
  render(){
    const {primary, primaryText, navigation} = this.props;
    return (
      <View flex center>
        <FlatList
          data={[
            {key: 'Weather'}, 
            {key: 'Basic List'},
            {key: 'Category List'},
          ]}
          renderItem={this._renderListItem}
        />
      </View>
    )
  }
  _renderListItem = ({ item }) => {
    return (
      <ListItem
        onPress={() => this.props.navigation.navigate(item.key)}
        animation="fadeIn"
        easing="ease-out-expo"
        useNativeDriver
      >
      <ListItem.Part containerStyle={[styles.border, {paddingLeft: 17}]}>
          <ListItem.Part containerStyle={{marginBottom: 3}}>
            <Text dark10 text70 style={{flex: 1, marginRight: 10}} numberOfLines={1}>{item.key}</Text>
          </ListItem.Part>
        </ListItem.Part>
      </ListItem>
    )
  }
}

const styles = StyleSheet.create({
  border: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: ThemeManager.dividerColor,
  },
});