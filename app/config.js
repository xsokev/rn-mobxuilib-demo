const App = require('../app.json');

export const APPLICATION_TITLE    = "React Native Mobx App";
export const APPLICATION_PREFIX   = App.name;
export const SESSIONTIMEOUT       = 10 * 60 * 1000;
export const DATE_FORMAT          = "M/D/YYYY";
export const DATE_TIME_FORMAT     = "M/D/YYYY h:mm:ss A";
export const TIME_FORMAT          = "h:mma";

export const BASEURL              = "https://my.api.mockaroo.com";
export const API_KEY              = 'c873dff0';
export const REQUEST_TIMEOUT      = 20000;
export const API_PEOPLE           = () => `${BASEURL}/people.json?key=${API_KEY}`;
export const API_SPORTS           = () => `${BASEURL}/af.json?key=${API_KEY}`;

export const DEFAULT_FILTER_PLACEHOLDER = "filter events";
