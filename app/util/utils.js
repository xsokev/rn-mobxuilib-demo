import { Platform, PixelRatio } from 'react-native';
import { colors, shade, width, height } from '@uikit/styles';
import defaults from '@uikit/theme';

const defaultTextBase = 16;
const defaultImageBase = 100;

export const IS_SMALL_SCREEN = width === 320;
export const RADIUS = PixelRatio.getPixelSizeForLayoutSize(defaults.fabButtonRadius);
export const RADIUS_MINI = PixelRatio.getPixelSizeForLayoutSize(defaults.fabButtonRadiusMini);

export const titleCase = (str) => {
  const len = str.length;
  if(len === 2){
    return str.toUpperCase();
  } else {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
  }
}
export const listToString = (list) => {
  const len = list.length;
  if(len === 0){
    return '';
  } else if(len === 1) {
    return list[0];
  } else if(len === 2) {
    return list.join(' and ');
  } else {
    return list.slice(0, len - 1).join(', ') + ' and ' + list[len - 1];
  }
}
export const justify = (props) => {
  return props.justify ||
    props.between ? 'space-between' :
      props.around ? 'space-around' :
        props.center ? 'center' :
          props.end ? 'flex-end' :
            'flex-start'
}
export const align = (props) => {
    return props.align || 'flex-start';
}
export const backgroundColor = (props, def) => {``
  if (props.color) {
    return props.color;
  } else if (props.white) {
    return defaults.white;
  } else if (props.black) {
    return defaults.black;
  } else if (props.primary) {
    return defaults.primary;
  } else if (props.success) {
    return defaults.success;
  } else if (props.danger) {
    return defaults.danger;
  } else if (props.dark) {
    return defaults.dark;
  } else if (props.warning) {
    return defaults.warning;
  } else if (props.info) {
    return defaults.info;
  } else if (props.light) {
    return defaults.light;
  } else if (props.clear) {
    return defaults.clear;
  }
  return def || defaults.default || colors.clear;
}
export const color = (props, def) => {
  if (props.color) {
    return props.color;
  } else if (props.white) {
    return defaults.white;
  } else if (props.black) {
    return defaults.black;
  } else if (props.muted) {
    return defaults.muted;
  } else if (props.primary) {
    return defaults.primary;
  } else if (props.success) {
    return defaults.success;
  } else if (props.danger) {
    return defaults.danger;
  } else if (props.dark) {
    return defaults.dark;
  } else if (props.warning) {
    return defaults.warning;
  } else if (props.info) {
    return defaults.info;
  } else if (props.light) {
    return defaults.light;
  } else if (props.clear) {
    return defaults.clear;
  }
  return def || colors.black;
}
export const borderColor = (props, def) => {
  if (props.color) {
    return props.color;
  } else if (props.clear) {
    return defaults.clear;
  } else if (props.white) {
    return defaults.white;
  } else if (props.black) {
    return defaults.black;
  } else if (props.primary) {
    return shade(defaults.primary, 2);
  } else if (props.success) {
    return shade(defaults.success, 2);
  } else if (props.danger) {
    return shade(defaults.danger, 2);
  } else if (props.dark) {
    return shade(defaults.dark, 2);
  } else if (props.warning) {
    return shade(defaults.warning, 2);
  } else if (props.info) {
    return shade(defaults.info, 2);
  } else if (props.light) {
    return shade(defaults.light, 2);
  }
  return def || shade(defaults.default || colors.clear, 2);
}
export const padding = (props) => {
  if (props.nopad) {
    return 0;
  } else if (props.padding) {
    return props.padding;
  } else if (props.xsmall) {
    return defaults.padding * 0.5;
  } else if (props.small) {
    return defaults.padding * 0.75;
  } else if (props.large) {
    return defaults.padding * 2;
  } else if (props.xlarge) {
    return defaults.padding * 3;
  }
  return defaults.padding * 1.25;
}
export const normalize = (size) => {
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(size))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size)) - 2
  }
}
export const textSize = (props) => {
  if (props.size) {
    return props.size;
  } else if (props.xsmall) {
    return defaults.textXSmall || defaultTextBase - 4;
  } else if (props.small) {
    return defaults.textSmall || defaultTextBase - 2;
  } else if (props.large) {
    return defaults.textLarge || defaultTextBase + 2;
  } else if (props.xlarge) {
    return defaults.textXLarge || defaultTextBase + 4;
  }
  return defaults.textNormal || defaultTextBase;
}
export const textWeight = (props) => {
  if (props.thin) {
    return defaults.textWeightThin || 100;
  } else if (props.bold) {
    return defaults.textWeightBold || 600;
  }
  return defaults.textWeightNormal || 400;
}
export const textAlign = (props) => {
  if (props.right) {
    return 'right';
  } else if (props.center) {
    return 'center';
  } else if (props.justify) {
    return 'justify';
  }
  return 'left';
}
export const setXPos = ({ offsetX, mini, ...props }) => {
  const center = ((props.width || width) - (mini ? RADIUS_MINI : RADIUS)) / 2;
  const right = (props.width || width) - (mini ? RADIUS_MINI : RADIUS);
  if (props.center) {
    return center;
  } else if (props.right) {
    return right - ((offsetX || 0) + defaults.margin);
  } else {
    return (offsetX || 0) + defaults.margin;
  }
}
export const setYPos = ({ offsetY, mini, ...props }) => {
  const middle = ((props.height || height) - (mini ? RADIUS_MINI : RADIUS)) / 2;
  const bottom = (props.height || height) - (mini ? RADIUS_MINI : RADIUS);
  if (props.middle) {
    return middle;
  } else if (props.bottom) {
    return bottom - ((offsetY || 0) + defaults.margin);
  } else {
    return (offsetY || 0) + defaults.margin;
  }
}
export const imageSize = (props) => {
  if (props.size) {
    return props.size;
  } else if (props.xsmall) {
    return defaults.imageXSmall || `${defaultImageBase - 50}%`;
  } else if (props.small) {
    return defaults.imageSmall || `${defaultImageBase - 25}%`;
  } else if (props.large) {
    return defaults.imageLarge || `${defaultImageBase}%`;
  } else if (props.xlarge) {
    return defaults.imageXLarge || `${defaultImageBase + 50}%`;
  }
  return defaults.imageNormal || `${defaultImageBase}%`;
}
export const prepDate = (date) => {
  return date.replace(/Z/, '')
}
