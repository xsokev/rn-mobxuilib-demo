import {Screen as HomeScreen} from './screens/Home';
import {Screen as AboutScreen} from './screens/About';
import {Screen as WeatherScreen} from './screens/Weather';
import {
  BasicList as BasicListScreen,
  CategoryList as CategoryListScreen,
} from './screens/List';

export default {
  Home: { screen: HomeScreen },
  About: { screen: AboutScreen },
  Weather: { screen: WeatherScreen },
  "Basic List": { screen: BasicListScreen },
  "Category List": { screen: CategoryListScreen },
}
