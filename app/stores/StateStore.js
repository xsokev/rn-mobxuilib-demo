import { observable, action, computed, observe } from 'mobx';

export default class StateStore {
  Model = null;
  APIRequests = {};
  api = null;
  storage = null;
  ui = null;
  @observable state = 'initiating';

  constructor(apiTransport, storageTransport, ui, Model, APIRequests) {
    this.api = apiTransport;
    this.storage = storageTransport;
    this.ui = ui;
    this.Model = Model;
    this.APIRequests = APIRequests;
    observe(this.ui, 'isConnected', (change) => {
      if(change.oldValue === false && change.newValue === true){
        this.ready && this.load();
      }
    });
    if (this.state === 'initiating' && this.ui.isConnected) {
      this.load();
    }
  }
  @action load() {
    this.state = 'loading';
  }
  @action refresh() {
    if(this.ui.isConnected){
      this.state = 'refreshing';
    }
  }
  @computed get loading() {
    return this.state === 'loading';
  }
  @computed get refreshing() {
    return this.state === 'refreshing';
  }
  @computed get ready() {
    return this.state === 'done';
  }
  @computed get empty() {
    return this.state === 'done' && (this.items && this.items.length === 0 || !this.data);
  }
}
