import { AsyncStorage } from 'react-native';
import { APPLICATION_PREFIX } from '../../config';

export const remove = (key) => {
  return AsyncStorage.removeItem(`${APPLICATION_PREFIX}:${key}`);
}
export const save = (key, v) => {
  return AsyncStorage.setItem(`${APPLICATION_PREFIX}:${key}`, v);
}
export const load = (key, def) => {
  return AsyncStorage.getItem(`${APPLICATION_PREFIX}:${key}`);
}
export const loadAll = (handler) => {
  AsyncStorage.getAllKeys((err, keys) => {
    AsyncStorage.multiGet(keys, handler);
  });
}
