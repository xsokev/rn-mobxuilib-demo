// import axios from './api_mock';
import axios from 'axios';
import {BASEURL, REQUEST_TIMEOUT, API_KEY} from '../../config';

axios.defaults.baseUrl = BASEURL;
axios.defaults.headers = {
  'X-Requested-With': 'XMLHttpRequest:RESTAPI',
  'x-api-key': API_KEY
};
axios.defaults.timeout = REQUEST_TIMEOUT;
axios.defaults.validateStatus = (status) => {
  return status >= 200 && status < 300;
};

export const get = (url) => {
  return axios.get(url).then((response) => {
    return response && response.data;
  })
  .catch((error) => {
    throw error;
  });
}
export const post = (url, data) => {
  return axios.post(url, data)
  .then((response) => {
    return response && response.data;
  })
  .catch((error) => {
    throw error;
  });
}
export const del = (url) => {
  return axios.delete(url)
  .catch((error) => {
    throw error;
  });
}
export const put = (url, data) => {
  return axios.put(url, data).catch((error) => {
    throw error;
  });
}
