import { PushNotificationIOS } from 'react-native';

export const requestPermissions = (alert, badge, sound) => {
  return PushNotificationIOS.requestPermissions({
    alert: alert || true,
    badge: badge || true,
    sound: sound || true
  });
}
export const checkPermissions = (callback) => {
  return PushNotificationIOS.checkPermissions(callback);
}
export const scheduleLocal = (details) => {
  return PushNotificationIOS.scheduleLocalNotification(details);
}
export const cancelAllLocal = () => {
  return PushNotificationIOS.cancelAllLocalNotifications();
}
export const removeAllDelivered = () => {
  return PushNotificationIOS.removeAllDeliveredNotifications();
}
export const removeDelivered = (ids) => {
  return PushNotificationIOS.removeDeliveredNotifications(ids);
}
export const getDelivered = (callback) => {
  return PushNotificationIOS.getDeliveredNotifications(callback);
}
export const setAppBadgeNumber = (num) => {
  return PushNotificationIOS.setApplicationIconBadgeNumber(num);
}
export const getAppBadgeNumber = (callback) => {
  return PushNotificationIOS.getApplicationIconBadgeNumber(callback);
}
export const getScheduledLocal = (callback) => {
  return PushNotificationIOS.getScheduledLocalNotifications(callback);
}
export const addEventListener = (type, handler) => {
  return PushNotificationIOS.addEventListener(type, handler);
}
export const removeEventListener = (type, handler) => {
  return PushNotificationIOS.removeEventListener(type, handler);
}
