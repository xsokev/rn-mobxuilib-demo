import {API_PEOPLE, API_SPORTS} from '../config';
import UiStore from "./Ui";
import ThemeStore from "./Theme";

import {Store as HomeStore} from "../screens/Home";
import {Store as AboutStore} from "../screens/About";
import {Store as WeatherStore} from "../screens/Weather";
import {Store as ListStore, PersonModel, SportsPersonModel} from "../screens/List";

import * as apiTransport from './transports/api';
import * as storageTransport from "./transports/storage";

const theme = new ThemeStore();
const ui = new UiStore(theme, storageTransport);
const home = new HomeStore(apiTransport, storageTransport, null, ui);
const about = new AboutStore(apiTransport, storageTransport, null, ui);
const weather = new WeatherStore(apiTransport, storageTransport, ui);
const people = new ListStore(PersonModel, {load: API_PEOPLE()}, apiTransport, storageTransport, ui);
const sports = new ListStore(SportsPersonModel, {load: API_SPORTS()}, apiTransport, storageTransport, ui);

export {
  ui,
  theme,
  home,
  about,
  weather,
  people,
  sports,
}
