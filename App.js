import React from 'react';
import { Platform } from 'react-native';
import { Provider } from 'mobx-react/native';
import { StackNavigator } from 'react-navigation';
import { View, Assets } from 'react-native-ui-lib';

import {StatusBarCover} from '@uikit';

import * as stores from './app/stores';
import ROUTES from './app/routes';

const Navigator = StackNavigator(ROUTES, {
  initialRouteName: 'Home',
  // UNCOMMENT TO MAKE BACKGROUND COLOR TRANSPARENT AND USE A STATIC IMAGE
  // cardStyle: {
  //   backgroundColor: colors.clear,
  // },
  // transitionConfig: () => ({
  //   containerStyle: {
  //     backgroundColor: colors.clear,    
  //   }
  // }),
  navigationOptions: {
    headerStyle: {
      backgroundColor: stores.theme.primary
    },
    headerTintColor: stores.theme.primaryText
    // header: Header,    // UNCOMMENT TO USE A CUSTOM HEADER COMPONENT
  }
});
const navigator = (<Navigator />);

export default class App extends React.Component {
  render() {
    return (
      <Provider {...stores}>
        <View flex>
          { Platform.OS === 'ios' ? <StatusBarCover color={stores.theme.primary} /> : null }
          {navigator}
        </View>
      </Provider>
    );
  }
}
